var mongo = require('mongodb').MongoClient;
var client = require('socket.io').listen(4000).sockets;

// Connect to mongo
mongo.connect('mongodb://localhost/mongochat', function (err, db) {
  if (err) {
    throw err;
  }

  console.log('MongoDB connected...');

  // Connect to Server.io
  client.on('connection', function (socket) {
    var chat = db.db('mongochat').collection('chats');

    // Create function to send status
    var sendStatus = function(s) {
      socket.emit('status', s);
    };

    // Get chats from mongo collection
    chat.find().limit(100).sort({_id: 1}).toArray(function (err, res) {
      if (err) {
        throw err;
      }

      // Emit the messages
      socket.emit('output', res);
    });

    // Handle input events
    socket.on('input', function (data) {
      var name = data.name;
      var message = data.message;

      // Check for name and message
      if (!name || !message) {
        // Send error status
        sendStatus('Please enter a name and message.');
      } else {
        // Insert message
        chat.insert({name: name, message: message}, function () {
          client.emit('output', [data]);

          // Send status object
          sendStatus({
            message: 'Message sent',
            clear: true
          });

        });
      }
    });

    // Handle clear
    socket.on('clear', function () {
      console.log('Received cleared message');
      // Remove all chats from collection
      chat.remove({}, function () {
        console.log('Sending cleared confirm');
        client.emit('cleared');
      });
    });

  })

});